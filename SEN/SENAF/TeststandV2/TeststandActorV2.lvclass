﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"=&gt;MQ%!8143;(8.6"2CVM#WJ",7Q,SN&amp;(N&lt;!NK!7VM#WI"&lt;8A0$%94UZ2$P%E"Y.?G@I%A7=11U&gt;M\7P%FXB^VL\`NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAG_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y!#/7SO!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"+Z5F.31QU+!!.-6E.$4%*76Q!!$JQ!!!1&gt;!!!!)!!!$HQ!!!!&gt;!!!!!2B5:8.U=X2B&lt;G2"9X2P=F9S,GRW9WRB=X-!!!!!!!#A&amp;Q#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!%^*4U&lt;J03B/H9C2UI\D8?1!!!!-!!!!%!!!!!#A\M.8=05+4K10-F9(!9NFV"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!SZ,$&amp;UO&gt;-U[&gt;5A7M%0.[RA%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!"$Z!#D39]Y"..UO+(1D!C*H!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!)A!!!"JYH'.A9W"K9,D!!-3-1-T5Q01$S0\!Q-!!!&amp;;6"O9!!!!!!%=!!!%9?*RD9-!%`Y%!3$%S-$#&gt;!.)M;/*A'M;G*M"F,C[\I/,-5$?S1E1:A7*-?Y!-*J!=1C]D!]1`9$_B!$9M:A-!&gt;S-I&amp;A!!!!!-!!&amp;73524!!!!!!!$!!!":!!!!MRYH%NA:'$).,9QWQ#EG9&amp;9H+'")4E`*:7,!=BHA)!N4!Q5AQ#I?6JIYI9($K="A2[`@!O9X`S'J^N&amp;2;#Z2E7#K63EWU&gt;&amp;J..(B;742?8&amp;H````T=@Y4H=\:&amp;TX.%'J,;&lt;!SB_X%7&amp;!]1"UCQA_H^A"EA6T,Q!I'E=$28+$#5MBA?C$B^P-''%7!QT-AL6`BUA[=0)\O%!/E,AY%/7\E9.),^X)IA%#P&amp;UBH")((@BU"%$]BF0&gt;!+N\_3"O:)$&lt;H]9S)!3&amp;9&amp;/%Z",75#GA^6UMRVXU!#\WU%%1G6!K!I)61"W$.A&amp;2TDC$M0$;_XL?\N!Y=C'&amp;)9/1.Q!R+!Y2-:[$)Q-)!O:A'1N6+U.E-U%&amp;90&amp;"9B^!=L71.,T"=F]E"[1T"KI')C^#=JOA,I(*0983%_!ME'_49#SO9(M"6#W%*!N!'6,!NE0I'QZ+(M$.)JQU=\_,KZ)XI?H4Q!V(8+!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!"Y?!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!"YL+KLL(A!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!"YL+KDI[/DK[RY!!!!!!!!!!!!!!!!!!!!!0``!!"YL+KDI[/DI[/DI[OM?!!!!!!!!!!!!!!!!!!!``]!K[KDI[/DI[/DI[/DI[/LL!!!!!!!!!!!!!!!!!$``Q#KKK/DI[/DI[/DI[/DI`[L!!!!!!!!!!!!!!!!!0``!+KLK[KDI[/DI[/DI`\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OKI[/DI`\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLKKT_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OL`P\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#KK[OLK[OLK`\_`P\_`P[K!!!!!!!!!!!!!!!!!0``!+KLK[OLK[OL`P\_`P\_`KI!!!!!!!!!!!!!!!!!``]!KKOLK[OLK[P_`P\_`P\_KA!!!!!!!!!!!!!!!!$``Q#LK[OLK[OLK`\_`P\_`KOL!!!!!!!!!!!!!!!!!0``!!#EKKOLK[OL`P\_`KOMJ!!!!!!!!!!!!!!!!!!!``]!!!!!J+OLK[P_`KOLJ!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!+3LK[OLIQ!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!#EIQ!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!$!!"2F")5!!!!!!!!Q!!!I)!!!4M?*SNF%^I%U%9R&lt;]*7ZG%&amp;G&gt;D;RNI3&amp;)WM9AJ%D#N17WR5[&amp;;CJ)70&amp;7$'`^!-:+EYKG^,%)/P2D-1?AN"$RZ#-7\"#^\M#=&amp;?YD.S;/8IG!X[\?4\O:0-6\=Q\!*]`P?\(O0!:#_MT&amp;8%T9.)/Q18Z9-]+A[!;B&amp;+2Q`E:@!6MFP)--_9M!=878\LC9:.W"1V30U)C`!$^RN@D.@Q7N#W!&amp;O0=6]/-RDQ'F6(Z6P+H7GP"N8#A0W6"H]&lt;*MU88?6Q%_[L761%,2*;Z7DJ!G%BS2*G\C&gt;?JL7&amp;/N@&gt;Z4[R%CX!9TL1VGF@AYHIP1(-:+M1:FE\*'!)S?B7KWW)&lt;E&amp;2=1R:CSGD'.XS6I@RMNV@U[J8R#-2T#IMWPL]%#T;*X&gt;AHL2-VRHC#,XQP\EMKX&amp;!];=Y"K."H+Y(H/0$2B7[N?JDR[I8Q?'\GDPA1#JX;0G&amp;`/NR=P,6AJCLR?$5(134B#7R*^*!]ZLOOM_/!YHI#C3E/QELG!3Z):)QMU,$.J2&amp;0]2":&gt;K%`\Z^9V=0JU.:BY'([SH=LHAM_S4Z[F]/KCG]KG4)6XFOHP;-M!3%`W!%:!A$L&amp;/QT.1K641!VT&lt;[$6%2Z7[Q]HC;_+/[7L&lt;&gt;%OV&lt;&gt;YMGM&gt;`L4SS$!QH8(:NI6X9'":7:LU([CLMJ@^@W$DW;,/HM$!&amp;):DN5\\J&amp;N22W""3-:DKQ]QAM^66W"$KR'S&gt;@I7^@,+Q)6P,+7SJ6/LCU0/%5VC*E&amp;:BN4XTS$T#WK+^%I1B*GI,\O7/E^0^DRP/`5+4#D?J\_]&lt;W!++&lt;2EQQO?^H^AC4BOECX3"\7#K?#/&gt;J4PUM0&lt;:O9&amp;K3Q[J\5''X&amp;,?"/D9(R&amp;W9Z1!!!!!!!1!!!!A!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!;Q!!!!'!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!#I8!)!!!!!!!1!)!$$`````!!%!!!!!!!Y!!!!"!!9!5!!!!!%!!!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!VRIS*A!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$8'D)G!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!#I8!)!!!!!!!1!)!$$`````!!%!!!!!!!Y!!!!"!!9!5!!!!!%!!!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2=!A!!!!!!"!!5!!Q!!!1!!!!!!!!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!F&amp;Q#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!!!!!!!!!!!!1!!A!)!!!!"!!!!%!!!!!I!!!!!A!!"!!!!!!$!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!/-!!!&amp;B?*S.E%V+!U%1B&lt;_?6J.-`)E`#R'%8ICY=B%P-#*E(52=CMXUD!C.)^/6Y.*&lt;?AW^A:6*R%5WY5&amp;2^;LKP?I'DHHCZ_NM@!FEJQ^6EC4_,&gt;S7UL30Y_MY,[.0[73N55LEKJD#_6W=*;F;V^3OGX8P\?P=3_7#&amp;]_WAJQ!ZBODB6W6X'A[+(L9/L\1,TZN=S^,IGJK2KJN&gt;8S(HJW&amp;GHTB:?5D[0;5D$Y$,D&lt;ST_BE."BU\6GNNZCIC'/80358W&amp;?T0ZA6VJH`TE'(Z5--)V6%$^)PV#RHS#&amp;('I?`KTMZ&lt;Q!!!!"F!!%!!A!$!!1!!!")!!]%!!!!!!]!W!$6!!!!51!0"!!!!!!0!.A!V1!!!&amp;I!$Q1!!!!!$Q$9!.5!!!"DA!#%!)!!!!]!W!$6#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*#&amp;.F:W^F)&amp;6*!4!!!!"35V*$$1I!!UR71U.-1F:8!!!/H!!!""U!!!!A!!!/@!!!!!!!!!!!!!!!)!!!!$1!!!1)!!!!'UR*1EY!!!!!!!!"6%R75V)!!!!!!!!";&amp;*55U=!!!!!!!!"@%.$5V1!!!!!!!!"E%R*&gt;GE!!!!!!!!"J%.04F!!!!!!!!!"O&amp;2./$!!!!!!!!!"T%2'2&amp;-!!!!!!!!"Y%R*:(-!!!!!!!!"^&amp;:*1U1!!!!!!!!##(:F=H-!!!!%!!!#(&amp;.$5V)!!!!!!!!#A%&gt;$5&amp;)!!!!!!!!#F%F$4UY!!!!!!!!#K'FD&lt;$A!!!!!!!!#P%R*:H!!!!!!!!!#U%:13')!!!!!!!!#Z%:15U5!!!!!!!!#_&amp;:12&amp;!!!!!!!!!$$%R*9G1!!!!!!!!$)%*%3')!!!!!!!!$.%*%5U5!!!!!!!!$3&amp;:*6&amp;-!!!!!!!!$8%253&amp;!!!!!!!!!$=%V6351!!!!!!!!$B%B*5V1!!!!!!!!$G&amp;:$6&amp;!!!!!!!!!$L%:515)!!!!!!!!$Q!!!!!$`````!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!*!!!!!!!!!!!`````Q!!!!!!!!$)!!!!!!!!!!$`````!!!!!!!!!.Q!!!!!!!!!!0````]!!!!!!!!!Z!!!!!!!!!!!`````Q!!!!!!!!%1!!!!!!!!!!$`````!!!!!!!!!2A!!!!!!!!!!0````]!!!!!!!!"1!!!!!!!!!!!`````Q!!!!!!!!'-!!!!!!!!!!$`````!!!!!!!!!:Q!!!!!!!!!"0````]!!!!!!!!$"!!!!!!!!!!(`````Q!!!!!!!!-9!!!!!!!!!!D`````!!!!!!!!!SA!!!!!!!!!#@````]!!!!!!!!$0!!!!!!!!!!+`````Q!!!!!!!!.-!!!!!!!!!!$`````!!!!!!!!!W!!!!!!!!!!!0````]!!!!!!!!$?!!!!!!!!!!!`````Q!!!!!!!!/-!!!!!!!!!!$`````!!!!!!!!""!!!!!!!!!!!0````]!!!!!!!!)&amp;!!!!!!!!!!!`````Q!!!!!!!!AE!!!!!!!!!!$`````!!!!!!!!#KQ!!!!!!!!!!0````]!!!!!!!!+N!!!!!!!!!!!`````Q!!!!!!!!K]!!!!!!!!!!$`````!!!!!!!!#MQ!!!!!!!!!!0````]!!!!!!!!,.!!!!!!!!!!!`````Q!!!!!!!!M]!!!!!!!!!!$`````!!!!!!!!$/Q!!!!!!!!!!0````]!!!!!!!!-^!!!!!!!!!!!`````Q!!!!!!!!T]!!!!!!!!!!$`````!!!!!!!!$3A!!!!!!!!!A0````]!!!!!!!!/%!!!!!!56'6T&gt;(.U97ZE17.U&lt;X*7-CZD&gt;'Q!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2B5:8.U=X2B&lt;G2"9X2P=F9S,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!!!!#!!%!!!!!!!!!!!!!!1!'!&amp;!!!!!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!")!A!1!!!!!!!!#``]!!!!"!!!!!!!"!!!!!!%!"A"1!!!!!1!!!!!!!@````Y!!!!!!2&amp;#98.F17.U&lt;X)O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!3!)!%!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.1</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!\!!!!!2&amp;#98.F17.U&lt;X)O&lt;(:D&lt;'&amp;T=V"53$!!!!!&gt;!!%!"!!!"%*B=W521G&amp;T:5&amp;D&gt;'^S,GRW9WRB=X-!!!!!</Property>
	<Item Name="TeststandActorV2.ctl" Type="Class Private Data" URL="TeststandActorV2.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="API" Type="Folder">
		<Item Name="AppMgrStop.vi" Type="VI" URL="../AppMgrStop.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'2!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=Q!!&amp;&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!)1V&amp;&lt;76S:W6O9XF4&gt;'^Q!&amp;!!]&gt;=;M:1!!!!#'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=R*"=(".:X*4&gt;'^Q2'&amp;U93ZD&gt;'Q!(%"1!!%!"QZ"=(".:X*4&gt;'^Q2'&amp;U91!!/%"Q!"Y!!"I96'6T&gt;(.U97ZE17.U&lt;X*7-CZM&gt;G.M98.T!!!46'6T&gt;(.U97ZE17.U&lt;X*7-C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!A!#1)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!E!!!!!!"!!I!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="FindSeqFile.vi" Type="VI" URL="../FindSeqFile.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%P!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=Q!!&amp;&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!;'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=Q!!%V2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="LogIn.vi" Type="VI" URL="../LogIn.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;"!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=Q!!&amp;&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"*!-0````])68.F=EZB&lt;75!!$B!=!!?!!!;'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=Q!!%V2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!##!!!!*!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
		</Item>
		<Item Name="NewExecution.vi" Type="VI" URL="../NewExecution.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(2!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=Q!!&amp;&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!-0````]2=W6R&gt;76O9W6/97VF5'&amp;S97U!(5!$!":F?'6D&gt;82J&lt;WZ5?8"F47&amp;T;V"B=G&amp;N!!!;1$$`````%&amp;"S&lt;W.F=X..&lt;W2F&lt;&amp;"B&gt;'A!!&amp;%!]&gt;&gt;M]^Y!!!!#'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=R&amp;&amp;?'6D&gt;82J&lt;WZ%982B,G.U&lt;!!?1&amp;!!!Q!(!!A!#1V&amp;?'6D&gt;82J&lt;WZ%982B!$B!=!!?!!!;'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=Q!!%V2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!+!!M#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!#!!!!*!!!!!!!1!-!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1350574596</Property>
		</Item>
		<Item Name="OpenSequenceFile.vi" Type="VI" URL="../OpenSequenceFile.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'_!!!!$!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=Q!!&amp;&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"J!-0````]15W6R&gt;76O9W6';7RF5'&amp;U;!!!%E!Q`````QF.&lt;W2F&lt;&amp;"B&gt;'A!9Q$RVT6S$A!!!!)96'6T&gt;(.U97ZE17.U&lt;X*7-CZM&gt;G.M98.T'V.F=86F&lt;G.F2GFM:5FO:G^S&lt;7&amp;U;7^O,G.U&lt;!!G1&amp;!!!A!(!!A85W6R&gt;76O9W6';7RF37ZG&lt;X*N982J&lt;WY!/%"Q!"Y!!"I96'6T&gt;(.U97ZE17.U&lt;X*7-CZM&gt;G.M98.T!!!46'6T&gt;(.U97ZE17.U&lt;X*7-C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!E!#A)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!E!!!!!!"!!M!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
		</Item>
		<Item Name="UIMessageHandler.vi" Type="VI" URL="../UIMessageHandler.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!)*!!!!$A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=Q!!&amp;&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!N!!Q!&amp;28:F&lt;H1!%5!+!!N/&gt;7VF=GFD2'&amp;U91!51$$`````#F.U=GFO:U2B&gt;'%!!%B!=!!(!1)!!!!"MHF/]]#W%&gt;#4H!!ALWDIEQ!!!!2E*^$AZ"!2U*/U!##P;/C4!!!!!1!!!!!05W6R&gt;76O9W6$&lt;WZU:8BU!')!]&gt;=RQJ%!!!!#'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=RB635VF=X.B:W6*&lt;G:P=GVB&gt;'FP&lt;CZD&gt;'Q!+%"1!!1!"Q!)!!E!#B2635VF=X.B:W6*&lt;G:P=GVB&gt;'FP&lt;A!!/%"Q!"Y!!"I96'6T&gt;(.U97ZE17.U&lt;X*7-CZM&gt;G.M98.T!!!46'6T&gt;(.U97ZE17.U&lt;X*7-C"J&lt;A"5!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!M!$!)!!(A!!!U)!!!!!!!!!!!!!)U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!)!!!!E!!!!!!"!!U!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
	</Item>
	<Item Name="TypeDef" Type="Folder">
		<Item Name="AppMgrInitData.ctl" Type="VI" URL="../AppMgrInitData.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="AppMgrStopData.ctl" Type="VI" URL="../AppMgrStopData.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="ExecutionData.ctl" Type="VI" URL="../ExecutionData.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="SequenceFileInformation.ctl" Type="VI" URL="../SequenceFileInformation.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="UIMessageInformation.ctl" Type="VI" URL="../../../Classes/TestStandV2/TypeDefs/UIMessageInformation.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$C!!!!"1!,1!-!"56W:7ZU!"&amp;!#A!,4H6N:8*J9U2B&gt;'%!&amp;%!Q`````QJ4&gt;(*J&lt;G&gt;%982B!!")1(!!"Q%#!!!!!&lt;*Z4P0!NB(1EZQ!)+^I[*-!!!!%:#@1Y/11%&gt;#4N!!ALWDIEQ!!!!%!!!!!$V.F=86F&lt;G.F1W^O&gt;'6Y&gt;!"C!0(8-=+2!!!!!BB5:8.U=X2B&lt;G2"9X2P=F9S,GRW9WRB=X-965F.:8.T97&gt;F37ZG&lt;X*N982J&lt;WYO9X2M!#B!5!!%!!!!!1!#!!-565F.:8.T97&gt;F37ZG&lt;X*N982J&lt;WY!!!%!"!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">3145728</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082143232</Property>
		</Item>
	</Item>
	<Item Name="SetStationGlobal.vi" Type="VI" URL="../SetStationGlobal.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#Q!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)O&lt;(:D&lt;'&amp;T=Q!!&amp;&amp;2F=X2T&gt;'&amp;O:%&amp;D&gt;'^S6D)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!-0````]&amp;6G&amp;M&gt;75!$E!Q`````Q2/97VF!!!Y1(!!(A!!'BB5:8.U=X2B&lt;G2"9X2P=F9S,GRW9WRB=X-!!".5:8.U=X2B&lt;G2"9X2P=F9S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!=!#!!*!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!##!!!!AA!!!#1!!!!!!%!#A!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1073741824</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821056</Property>
	</Item>
</LVClass>
