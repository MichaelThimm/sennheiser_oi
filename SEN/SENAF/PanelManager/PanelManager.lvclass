﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"7F5F.31QU+!!.-6E.$4%*76Q!!%9Q!!!1:!!!!)!!!%7Q!!!!:!!!!!22197ZF&lt;%VB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!!!!!!+!8!)!!!$!!!#A!!!!!!!!%!!-!0!#]!"^!A!)!!!!!!1!"!!&lt;`````!!!!!!!!!!!!!!!!5BH)KT&lt;%CE[L&amp;C":DI#7RQ!!!!Q!!!!1!!!!!+R)BQ&gt;&amp;"PZ-A_K8:%BO&amp;Z&lt;5(9T:DQ#S"/G!#:DM_%*_!!!1!!!!!!#&lt;+$J$&gt;&lt;+P2:`@HR:S%/G&lt;!1!!!0`````5(9T:DQ#S"/G!#:DM_%*_!!!!%#8+W:99L/II,&lt;I2*"@Y7&amp;!!!!!%!!!!!!!!!#=!!5R71U-!!!!"!!*735R#!!!!!&amp;"53$!!!!!&amp;!!%!!1!!!!!#!!-!!!!!!A!"!!!!!!!I!!!!+HC=9_"G9'JAO-!!R)R!T.4!^!0)`A$&amp;&amp;RA%/#!E!Q-!_+),+Q!!!&amp;!!!!%=?*RD9-!%`Y%!3$%S-$$&gt;!.)M;/*A'M;G*M"C,N!.T#J!GB78?C$&amp;$(5D6!UD5)RJ$Z$""*+$KO'!S$&amp;&gt;!/)4[/;QI!N!Q(E!:]MF-Q!!!!Q!!6:*2&amp;-!!!!!!!-!!!(#!!!$:(C=O]$)Q*"J&lt;''GQ-4!Q!RECT-U-#4HJ[2S-1$Z$"$A!W.1!!+AZGGBC2M?/*Q'"(L]]CVA@P-&lt;HGY8&amp;9(G'B5*JF+2&lt;B]6E5Y@&amp;::/&amp;J58@`\``^^]B/&gt;QNU@/=5=&lt;E.JO$K$Y=2=6$B!(3,/![0_"'3"6-0-#A+:R.&amp;1I-Z3Q'"[)/HS]Q9129D(-S#B5_]/;XX!!N5BUF[A)&gt;,&lt;J!*U-&gt;).-:Z]"C-81T9HCGB_-`&amp;-/F$JUA^8V?L"U^I%VA"8X"L*U4A0L=G$J^G4"U(?1@^N*@J=4H3&gt;?-`.P/^"]B#-/['9("J$V0CI#"R_S&gt;$&gt;K!.X4/R&amp;%!I6Y/E-Y*)[\=/C)!@G-*TK"XOXEA95+RW%5^Y0=&lt;A)+'2;1:]&amp;KONG//WC!Q]F""%*F1+A+#&amp;5!IH;!9Q$E&amp;(D]L(V^&lt;R=LE':$CD-()'Y!9F#;1=:[$)Q-)!O:A'1N6+U.E-U%&amp;90&amp;09DN!)W'&amp;UB[0E$F8S+*\9#+;3#*J4!CX!%S'S1D!R5$M:7A\!;IOU&amp;CO5#R#6"W#:#&gt;!'68!^E#D""W%YA.&amp;?]%MA/AYHV1.MC0$!SY;7&gt;`&amp;V?E9),H'Q$D2ZH#!!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!!Q8!)!!!!!%-4=O-!!!!!!/&amp;Q'!%!!!"D%X,D!O-1!!!!!!!"1"!!!!^6=VAHEGIIQO=V*/"EEZH1!!!!U!!!!!!!!!!!!!!!!!!!!!!!!!A0````_!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9"A!!'"G!!"BA9!!:A"A!'A!%!"M!$!!;Q$1!'D$M!"I06!!;!KQ!'A.5!"I#L!!;!V1!'A+M!"I$6!!:ALA!''.A!"A&lt;A!!9"A!!(`````!!!%!0```````````````````````````````````````````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!,GZ!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!,H2R=P2O1!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!,H2R&lt;_`P\`,U&lt;E!!!!!!!!!!!!!!!!!!!!!``]!!,H2R&lt;_`P\_`P\_`S^'Z!!!!!!!!!!!!!!!!!!$``Q$,R&lt;_`P\_`P\_`P\_`P]P2!!!!!!!!!!!!!!!!!0``!-8&amp;P\_`P\_`P\_`P\_``]M!!!!!!!!!!!!!!!!!``]!R=P,R&lt;_`P\_`P\_`````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]7`P\_````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P&amp;U@```````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-8,S]P,S]P,`````````]5!!!!!!!!!!!!!!!!!``]!R=P,S]P,S]P`````````R1!!!!!!!!!!!!!!!!$``Q$&amp;S]P,S]P,S``````````&amp;!!!!!!!!!!!!!!!!!0``!-P,S]P,S]P,````````S]M!!!!!!!!!!!!!!!!!``]!!-8&amp;S]P,S]P`````S^(&amp;!!!!!!!!!!!!!!!!!!$``Q!!!!$&amp;S]P,S```S]P&amp;!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!R=P,S]O`!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!-7`!!!!!!!!!!!!!!!!!!!!!!!!!!$```````````````````````````````````````````]!!!!-!!&amp;'5%B1!!!!!!!$!!!$M1!!#1VYH+W747A4124(XS3&lt;-FMNTF;L"KS*96N&amp;,.2+L&gt;;WNH;.6EN29]7&lt;2N/K5)TW1TSV)0'D9KU;D+"Y\5GQU).X#&lt;XM15]?^"!NXF4U5B4M:HUTG]VGUS`&amp;*D!MS@T@R``^:FA!779&lt;06E9.I#Q78TI.+!UJB/!&gt;!W&amp;X+@[&amp;L"O]BP)/D]RI*6WMQ_?,.FEQ/K98EVLN6(YA&lt;P.D_9D&lt;VS[TT\BVB,GRW#F"KS*[2O5)WK'K3]XK;-_/[I#F7S-:$WHV="0/J;)9U*)&lt;//L5E/S1,1NEJ1)(9N?\EGI`&amp;?ZBPJ&amp;3.E!JOFF`7JG+U&lt;%V+^&amp;3/G,&gt;VC;MU-#BNQ'5V.4DECR2.7CD$WI]1Y$?-?F,UNISD7^=E$.\"#;5K("0/.W(CW14@,;O;B9OF&lt;4'5J2&gt;TX8-N@;/K.6[':G:F#(;UZXU9"V;O9!^&gt;.0M@?_MO/*6U#!J-^1=UNWGOO6,DY&amp;M&lt;=="V(63&amp;A%HS-'&lt;%`IHL-AW@9_A[19AW30I1H(1-*C$,)WSM#:1X+:/7B3/F4:XD=U-.D4(YTX"M`X21='AF@[,VW,$P9%9^("[0Q*.7O[X-#\Z]E%(&amp;!"%DS"P96ORW&amp;C9A).Q.72NK"UAZL*[R42480?]:DD/-`K/,=@H&gt;._H&lt;T!X;NK^.D-?A3T&lt;7)&gt;==CN1X)66FS=C^T[F3&gt;X.Q)V8%1OV--.##^"99-F=MD&amp;`1"*K&amp;^#MQ=V)Y8EIK9?.?(FS&gt;U\DVSO,3)XF5KZ&gt;/B`9ZZ=C2#,X-2&lt;=][=Y`R_.H`#")1&amp;PR[B[-:R;(J1A&amp;C,/;ZC$G[`T\&lt;`ZI,%%P4@+?&lt;RAA-)FI#&amp;,9U-H?/$[*M`C6O;4H?KG1A'ZY%XAQ&gt;;),39I[M-O'UB\?+S$&gt;2#$Z7!V6X/Y;^=&lt;$W&lt;D0LFA*0^DK&lt;\^\H0RXLQ12-%\.:-:=(ZI-_DAP/SER&lt;H8MYZPQ0?'(!8*^&gt;5'/]&amp;0(-D@=^"WP&lt;5`!:A:K&amp;B7;&lt;4)2HN$#\CZRBWN-P&gt;E2?`3@N[S8=U/4G*(?(K;/]DL#XC6-DWK2"+^^65B/GY*=*4)?&gt;/2&lt;)QXY+;"VDEWI):]E]+5E6XC[D1@&lt;=]&amp;*ZXVVG?3_AZD?4_29\&lt;=BS&lt;XV&amp;5#[K&lt;YU/,=BT[7YZ,4H5%4`4UTH@^],^3X,'3&amp;"`Z$YK0OCDW=5@^=F&gt;"Z@4$^&amp;$_"92'6-V=;A-\C'&amp;($+D1WMP@M!\M9$8NI!@:=\TN]?*@4Z`4W@3\`#N+OD/P4,QFT&lt;]F^7G!&lt;PQ$CL'=:A!!!!!!!!1!!!!Y!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!!M!!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!'Y8!)!!!!!!!1!)!$$`````!!%!!!!!!&amp;)!!!!$!"2!=!!)!!!!!A!!"F:*)&amp;*F:A!!&amp;E"Q!!A!!!""!!!)5X6C5'&amp;O:7Q!!#"!5!!#!!!!!22197ZF&lt;%VB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!!1!#!!!!!!!!!"N-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$7Y6&amp;;!!!!!!!!!#:-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;-98.U18"Q&lt;'FF:&amp;2J&lt;76T&gt;'&amp;N=!!!!"E8!)!!!!!!!1!&amp;!!=!!!%!!.&lt;B56I!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962Z='6%:8.D!!!!&lt;B=!A!!!!!!"!!A!-0````]!!1!!!!!!5A!!!!-!&amp;%"Q!!A!!!!#!!!'6EEA5G6G!!!71(!!#!!!!%%!!!B4&gt;7*197ZF&lt;!!!)%"1!!)!!!!"&amp;&amp;"B&lt;G6M47&amp;O97&gt;F=CZM&gt;G.M98.T!!!"!!)!!!!!!!!!(ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B5WF[:1!!!"E8!)!!!!!!!1!&amp;!!-!!!%!!!!!!!A!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U952G&lt;(2%982B!!!!9B=!A!!!!!!$!"2!=!!)!!!!!A!!"F:*)&amp;*F:A!!&amp;E"Q!!A!!!""!!!)5X6C5'&amp;O:7Q!!#"!5!!#!!!!!22197ZF&lt;%VB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!!1!#!!!!!!!!!!!!!!!!!!!!'ER71WRB=X.1=GFW982F2'&amp;U962B9E^S:'6S!!!!,2=!A!!!!!!#!!5!"Q!!$!"!!!(`````!!!!!1!"!!!!!A!!!!!!!!!"!!!!!!!!!!1!"1!.!!!!"!!!!*%!!!!I!!!!!A!!"!!!!!!#!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!3=!!!(P?*S.5=V/AU!9(.T31KUN6;T7HW30(IQ88Q"D9O,"J.&lt;%1U_OM$1GJ$15'I^^3O__A&lt;["QU*ID"@T:7&amp;HG*W&gt;DQ`!'@RA#1@!$N"_PJ&gt;4(1/DGLM"H+@C&gt;;)7/A&amp;G_0\Y@*S67N^1$WKBZDK\3N:BIF9L\R=:ZAEOAUHJ#_P].CF7O=ZE'EODF=PM&lt;;VS,3/6+^AM&gt;"&amp;2_16")'K);Q)X[%$%S2R/M"(J.+])H=&lt;Q[#`196:8&amp;&amp;'-(JJ?GDV\1.`EW%60Z/]2H5P5R2YO`JW0+BPN]G(RH_#&amp;BVKYIZ'%BS&amp;$F,6P'KF+V078W8YZ-&amp;5V;M'HY[9/:W05]%@=B?9_CWM\(%:S/2R:47&gt;)X',#0A9YR$('6)RZYI2YQ0=J]!.6$%;T!!!!!'5!!1!#!!-!"!!!!%A!$Q1!!!!!$Q$9!.5!!!"2!!]%!!!!!!]!W!$6!!!!7A!0"!!!!!!0!.A!V1!!!'/!!)1!A!!!$Q$9!.5)5W6H&lt;W5A65E)5W6H&lt;W5A65E)5W6H&lt;W5A65E"-!!!!&amp;*45E-.#A!$4&amp;:$1UR#6F=!!"'-!!!%'1!!!#!!!"&amp;M!!!!!!!!!!!!!!!A!!!!.!!!"!A!!!!&lt;4%F#4A!!!!!!!!&amp;54&amp;:45A!!!!!!!!&amp;I5F242Q!!!!!!!!&amp;]1U.46!!!!!!!!!'14%FW;1!!!!!!!!'E1U^/5!!!!!!!!!'Y6%UY-!!!!!!!!!(-2%:%5Q!!!!!!!!(A4%FE=Q!!!!!!!!(U6EF$2!!!!!!!!!))&gt;G6S=Q!!!!1!!!)=5U.45A!!!!!!!!+!2U.15A!!!!!!!!+535.04A!!!!!!!!+I;7.M/!!!!!!!!!+]4%FG=!!!!!!!!!,12F")9A!!!!!!!!,E2F"421!!!!!!!!,Y6F"%5!!!!!!!!!--4%FC:!!!!!!!!!-A1E2)9A!!!!!!!!-U1E2421!!!!!!!!.)6EF55Q!!!!!!!!.=2&amp;2)5!!!!!!!!!.Q466*2!!!!!!!!!/%3%F46!!!!!!!!!/96E.55!!!!!!!!!/M2F2"1A!!!!!!!!0!!!!!!0````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!$`````!!!!!!!!!-1!!!!!!!!!!0````]!!!!!!!!!W!!!!!!!!!!!`````Q!!!!!!!!$A!!!!!!!!!!$`````!!!!!!!!!1Q!!!!!!!!!!0````]!!!!!!!!"&amp;!!!!!!!!!!!`````Q!!!!!!!!&amp;!!!!!!!!!!!$`````!!!!!!!!!:1!!!!!!!!!!0````]!!!!!!!!"J!!!!!!!!!!%`````Q!!!!!!!!.M!!!!!!!!!!@`````!!!!!!!!!Y!!!!!!!!!!#0````]!!!!!!!!$E!!!!!!!!!!*`````Q!!!!!!!!/E!!!!!!!!!!L`````!!!!!!!!!\1!!!!!!!!!!0````]!!!!!!!!$S!!!!!!!!!!!`````Q!!!!!!!!0A!!!!!!!!!!$`````!!!!!!!!!`1!!!!!!!!!!0````]!!!!!!!!%?!!!!!!!!!!!`````Q!!!!!!!!B]!!!!!!!!!!$`````!!!!!!!!#)Q!!!!!!!!!!0````]!!!!!!!!-2!!!!!!!!!!!`````Q!!!!!!!!R-!!!!!!!!!!$`````!!!!!!!!$&amp;1!!!!!!!!!!0````]!!!!!!!!-:!!!!!!!!!!!`````Q!!!!!!!!T-!!!!!!!!!!$`````!!!!!!!!$.1!!!!!!!!!!0````]!!!!!!!!0G!!!!!!!!!!!`````Q!!!!!!!!_A!!!!!!!!!!$`````!!!!!!!!$[A!!!!!!!!!!0````]!!!!!!!!0V!!!!!!!!!#!`````Q!!!!!!!"%!!!!!!""197ZF&lt;%VB&lt;G&amp;H:8)O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!22197ZF&lt;%VB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!!!!!!!-!!1!!!!!!!!!!!!!"!!9!5!!!!!%!!!!!!!!!!!!!!1Z-97*73568)%^C;G6D&gt;!"16%AQ!!!!!!!!!!!!%A#!"!!!!!!!!!,``Q!!!!%!!!!!!!%!!!!!!1!'!&amp;!!!!!"!!!!!!!"`````A!!!!!"%5*B=W6"9X2P=CZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!")!A!1!!!!!!!!!!!!!!1!!!!!!!A!!!!!$!"2!=!!)!!!!!A!!"F:*)&amp;*F:A!!&amp;E"Q!!A!!!""!!!)5X6C5'&amp;O:7Q!!&amp;I!]&gt;&lt;B56I!!!!#&amp;&amp;"B&lt;G6M47&amp;O97&gt;F=CZM&gt;G.M98.T%&amp;"B&lt;G6M47&amp;O97&gt;F=CZD&gt;'Q!,%"1!!)!!!!"(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!A!!!!,``````````Q!!!!!!!!!!!!!!!!!!!2&amp;#98.F17.U&lt;X)O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!3!)!%!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.2</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!\!!!!!2&amp;#98.F17.U&lt;X)O&lt;(:D&lt;'&amp;T=V"53$!!!!!&gt;!!%!"!!!"%*B=W521G&amp;T:5&amp;D&gt;'^S,GRW9WRB=X-!!!!!</Property>
	<Item Name="PanelManager.ctl" Type="Class Private Data" URL="PanelManager.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="Properties" Type="Folder">
		<Item Name="SubPanel" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">SubPanel</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">SubPanel</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Write SubPanel.vi" Type="VI" URL="../Properties/Write SubPanel.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;#!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;&amp;"B&lt;G6M47&amp;O97&gt;F=CZM&gt;G.M98.T!!!15'&amp;O:7R.97ZB:W6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!71(!!#!!!!%%!!!B4&gt;7*197ZF&lt;!!!-%"Q!"Y!!"955'&amp;O:7R.97ZB:W6S,GRW9WRB=X-!!!^197ZF&lt;%VB&lt;G&amp;H:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">50331648</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268967936</Property>
			</Item>
		</Item>
		<Item Name="VIRef" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">VIRef</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">VIRef</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
			<Item Name="Write VI Ref.vi" Type="VI" URL="../Properties/Write VI Ref.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%T!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;&amp;"B&lt;G6M47&amp;O97&gt;F=CZM&gt;G.M98.T!!!15'&amp;O:7R.97ZB:W6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!51(!!#!!!!!)!!!:733"3:79!!$"!=!!?!!!7&amp;&amp;"B&lt;G6M47&amp;O97&gt;F=CZM&gt;G.M98.T!!!05'&amp;O:7R.97ZB:W6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!"!!!!#3!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">268967936</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="Actor Core.vi" Type="VI" URL="../Actor Core.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%&lt;!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!F&amp;5&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9AV"9X2P=CZM&gt;G.M98.T!!F"9X2P=C"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!$"!=!!?!!!7&amp;&amp;"B&lt;G6M47&amp;O97&gt;F=CZM&gt;G.M98.T!!!05'&amp;O:7R.97ZB:W6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"!!(!Q!!?!!!$1A!!!!!!!!!!!!!#1!!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!!!!!#1!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777600</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="ResizeMode.ctl" Type="VI" URL="../Typedef/ResizeMode.ctl">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
	</Item>
	<Item Name="ResizePanelVI.vi" Type="VI" URL="../ResizePanelVI.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'*!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$*!=!!?!!!7&amp;&amp;"B&lt;G6M47&amp;O97&gt;F=CZM&gt;G.M98.T!!!15'&amp;O:7R.97ZB:W6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1"&gt;!0(8"C_Y!!!!!B2197ZF&lt;%VB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=QZ3:8.J?G6.&lt;W2F,G.U&lt;!!R1"9!!Q2/&lt;WZF#6"B&lt;G6M6'^731F7362P5'&amp;O:7Q!!!J3:8.J?G6.&lt;W2F!!!Q1(!!(A!!&amp;B2197ZF&lt;%VB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!$V"B&lt;G6M47&amp;O97&gt;F=C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!E!!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#!!!!!!!!!!+!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
	</Item>
	<Item Name="Stop Core.vi" Type="VI" URL="../Stop Core.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$:!!!!"1!%!!!!-E"Q!"Y!!"955'&amp;O:7R.97ZB:W6S,GRW9WRB=X-!!""197ZF&lt;%VB&lt;G&amp;H:8)A&lt;X6U!!!81!-!%':J&lt;G&amp;M)'6S=G^S)'.P:'5!!$"!=!!?!!!7&amp;&amp;"B&lt;G6M47&amp;O97&gt;F=CZM&gt;G.M98.T!!!05'&amp;O:7R.97ZB:W6S)'FO!&amp;1!]!!-!!!!!!!!!!%!!!!!!!!!!!!!!!!!!A!$!Q!!?!!!!!!!!!!!!!!!!!!!C1!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"!!!!#1!!!!!!%!"!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
</LVClass>
