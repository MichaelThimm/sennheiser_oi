﻿<?xml version='1.0' encoding='UTF-8'?>
<LVClass LVVersion="17008000">
	<Property Name="NI.Lib.Description" Type="Str">Manage the container VI factory method and the use of the subpanel</Property>
	<Property Name="NI.Lib.HelpPath" Type="Str"></Property>
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;8"&lt;2MR%!813:!!O;K$1#V-#WJ",5Q,OPKI&amp;K9&amp;N;!7JA7VI";=JQVBZ"4F%#-ZG/O26X_ZZ$/87%&gt;M\6P%FXB^VL\_NHV=@X&lt;^39O0^N(_&lt;8NZOEH@@=^_CM?,3)VK63LD-&gt;8LS%=_]J'0@/1N&lt;XH,7^\SFJ?]Z#5P?=F,HP+5JTTF+5`Z&gt;MB$(P+1)YX*RU2DU$(![)Q3YW.YBG&gt;YBM@8'*\B':\B'2Z&gt;9HC':XC':XD=&amp;M-T0--T0-.DK%USWS(H'2\$2`-U4`-U4`/9-JKH!&gt;JE&lt;?!W#%;UC_WE?:KH?:R']T20]T20]\A=T&gt;-]T&gt;-]T?/7&lt;66[UTQ//9^BIHC+JXC+JXA-(=640-640-6DOCC?YCG)-G%:(#(+4;6$_6)]R?.8&amp;%`R&amp;%`R&amp;)^,WR/K&lt;75?GM=BZUG?Z%G?Z%E?1U4S*%`S*%`S'$;3*XG3*XG3RV320-G40!G3*D6^J-(3D;F4#J,(T\:&lt;=HN+P5FS/S,7ZIWV+7.NNFC&lt;+.&lt;GC0819TX-7!]JVO,(7N29CR6L%7,^=&lt;(1M4#R*IFV][.DX(X?V&amp;6&gt;V&amp;G&gt;V&amp;%&gt;V&amp;\N(L@_Z9\X_TVONVN=L^?Y8#ZR0J`D&gt;$L&amp;]8C-Q_%1_`U_&gt;LP&gt;WWPAO_0NB@$TP@4C`%`KH@[8`A@PRPA=PYZLD8Y![_ML^!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.6</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.LVClass.ClassNameVisibleInProbe" Type="Bool">true</Property>
	<Property Name="NI.LVClass.DataValRefToSelfLimitedLibFlag" Type="Bool">true</Property>
	<Property Name="NI.LVClass.FlattenedPrivateDataCTL" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!"C.5F.31QU+!!.-6E.$4%*76Q!!&amp;(!!!!1&gt;!!!!)!!!&amp;&amp;!!!!!&gt;!!!!!2B$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-!!!!!!!#A&amp;Q#!!!!Q!!!I!!!!!!!!"!!$!$Q!P!!@1)!#!!!!!!%!!1!'`````Q!!!!!!!!!!!!!!!,0O@55O2*"+LT_2Q_*A-E-!!!!-!!!!%!!!!!$/O^,?T##H1)21T(Z0,5*-V"W-W9]!MA4JA!G9\0B#@A!!%!!!!!!!/W&lt;]L0+*&amp;5;/6TA17&gt;ERH!%!!!$`````V"W-W9]!MA4JA!G9\0B#@A!!!""\.;TDZ\64&amp;6N'F^'35`TK!!!!"!!!!!!!!!!H!!&amp;-6E.$!!!!!1!#6EF-1A!!!!"16%AQ!!!!"1!"!!%!!!!!!A!$!!!!!!)!!1!!!!!!+A!!!#ZYH'0A:7"K9,D!!-3-1-T5Q01$S0Y!R2=9"$AA*!AS-!!!*J=,81!!!!!!5!!!!2RYH'.AQ!4`A1")-4)Q-,U!UCRIYG!;RK9GQ')OU!X-*E#;&amp;:&gt;[)-5-&gt;3.5$3.1D'E0E-%%EI/K%9$)-&gt;U!YB0IZG!VH)(B0!##5S6E!!!!$!!"6EF%5Q!!!!!!!Q!!!@E!!!0%?*QT9'*AS$3W-'M!UMS-$!TC$!U-S@EJK6Q-1$Y$"+S"-3A!!6$TN.$%$1]=4A-#08\Z&amp;D#`_1V0NYO+1(/.CA24K5CXDYJ)JY]+3S?,SIM`````&lt;T\#=\D&lt;)_?YIQV)&lt;4=(50SYCQI(C!/E75$U`]!-E#K9?1&amp;!UTA;+J1:3FA-$U1&gt;0NZAQACR'':E&amp;+L^Y7(.&lt;TC!?G3[3Y"7N^E!X1RUB%*HHQ/)R&gt;$.C?+=(YT]5Q\QNX;#($-.J,&lt;8C17K;2^)1[]`#V2H%)NV/!P`F%/FK&lt;U",*U1N9&amp;!"FD3A;8&lt;EQ84Y&amp;+\&lt;L":P2Z!5]#'AMV#VG@NS!*SQ&lt;94/U$?YH=^_*K8@^O"VRT]WQ[^:A;SGI^QR-5@2AF@$K"`"!Y_:/FOV!$S?S?#3+!14W=)B]2R&amp;QY&gt;-3#@]51H-$A\?7#BTA%04X$QF+A)&gt;*K!1JY&amp;:#N944@&lt;=1=.=$QYC%#I$!B6!;%+1.1/M!O!4DI-D`_VL_`N9A83&lt;%BJQA')'U#9#28L-4!SA#RE!J+V5,5W1$946!S7NE$M"&gt;"I@I'E2QAK^B**&lt;#&gt;5DQ;3W"&amp;'B$N!:I.E;K"C)(94F.U!&gt;4&gt;)\#R1&lt;!+5@18)4I#S&lt;Q0:"9Q1^C-A,1!6@QFE&lt;Y#+PY/S18ZE9-"./`O\O#)&amp;%TR@!A#8],N,!!!!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!-&amp;Q#!!!!!"$%X,D!!!!!!$B="A"!!!!9R.SYQ,D%!!!!!!!!5!1!!!068.9*Z*K+-,H.34A:*/:U!!!!.!!!!!!!!!!!!!!!!!!!!!!!!!)$`````A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!!!!"A!!!!9!!!!'!9!!"A:A!!99'!!'9!9!"I!"!!&lt;!!Q!'M!U!"IQ\!!;$V1!'A+M!"I$6!!;!KQ!'A.5!"I#L!!;!V1!'9+Y!"BD9!!9'Y!!'!9!!"`````Q!!"!$```````````````````````````````````````````]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!!&amp;"1!!!!!!!!!!!!!!!!!!!!!!!!!!``]!!!!!!!!&amp;L6G$L15!!!!!!!!!!!!!!!!!!!!!!!$``Q!!!!!&amp;L6EP,S]PA[U&amp;!!!!!!!!!!!!!!!!!!!!!0``!!!&amp;L6EP,S]P,S]P,Y/N"1!!!!!!!!!!!!!!!!!!``]!AVEP,S]P,S]P,S]P,S_$L1!!!!!!!!!!!!!!!!$``Q":73]P,S]P,S]P,S]P,`[$!!!!!!!!!!!!!!!!!0``!&amp;G$AVEP,S]P,S]P,`\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY.:,S]P,`\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$7;X_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY/$`P\_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AY0_`P\_`P\_71!!!!!!!!!!!!!!!!$``Q":AY/$AY/$A`\_`P\_`PZ:!!!!!!!!!!!!!!!!!0``!&amp;G$AY/$AY/$`P\_`P\_`FE!!!!!!!!!!!!!!!!!``]!79/$AY/$AY0_`P\_`P\_71!!!!!!!!!!!!!!!!$``Q#$AY/$AY/$A`\_`P\_`I/$!!!!!!!!!!!!!!!!!0``!!":79/$AY/$`P\_`I/N71!!!!!!!!!!!!!!!!!!``]!!!!!79/$AY0_`I/$71!!!!!!!!!!!!!!!!!!!!$``Q!!!!!!!&amp;G$AY/$,Q!!!!!!!!!!!!!!!!!!!!!!!0``!!!!!!!!!!":,Q!!!!!!!!!!!!!!!!!!!!!!!!!!````````````````````````````````````````````!!!!$!!"2F")5!!!!!!!!Q!!"']!!!N#?*SNFEVM'V51R_&gt;NVW%X;&gt;2VGL3V;0!3&lt;5+&amp;'B);B41GB$&lt;:F+:+1FP(U*&lt;QM=2OC24:E?UAR+'^7%A_^%+%$U%Z=-E*C9A=S)%$!AMJ7IHWV%/,:"JRYE.&amp;1B76WP5S\[X8OWP(.B^&gt;3U]L[`VHXPP.@U9,U$)K(?++=.5!)NX(FSE$GK-[!=DX#F"[?DY#+5)?!GE0%!./#"(J$F=EBQX9'^6\B(YV#X`A&lt;P-H]W0O1`Z([3ZO&lt;:)#'+T:A(V2`;$`D&amp;+1F#]/+VG@(&gt;50H&gt;*V5O1O+-'`B/O:"#;%T"'[_HN*%9D[.-^HOMZK]6B'I@_+P5+!B21.E&amp;3^.;E5HM')G0J&lt;&amp;N)XT'6^:_S1A#'0Q/&lt;GJC0S7[)?&gt;ITDK/'S!.SK&lt;\C/JEX6/V.+Y3D4.$-.ZFGV][D"YAI^/R663P?LOI23V(V1OD,6WDLD".0N\/SA$N?3\DU$WJ8#G"!1\E:PE^:TG;_!!-G`,:BP&amp;&lt;_E?P]-L1,&lt;WY;&amp;[!Y2+9TP91/?T?D=/]$&lt;?0_%&lt;VA:?,M-)VA'=IK6163T%DBV7'F1"Z80&gt;X7/,S[HUL'EH,AMTS^KK:3]F&amp;RY8UP(Z+C7VKIL^*+KCU0U^D1:-Q&gt;U!!`XY'MX\13ML[]D!&amp;Q&gt;[3B+$SK&amp;MMZ@OIV.0/I1JVE&gt;=C]D/@8"\"6+LTP%W:\FG7@\W(K;L1.M@&gt;.R]4&amp;UM6_K0+D(R9/0X]5PI,GO6LA9NO!W&lt;.&gt;RZ*!F=FS-_Q&amp;_B;U[GO/IO?:W-7KW5,0&gt;W-8$63[GWAI8ZX)ZDQZL%3K\G#@%=H(GJPH)@%3^`).ZC\4$,]T,FD]D7!Z6FZEJVT$(.O&lt;QO(&gt;_6`=3Z/]=ZJ0;^J6H9[FU/+X&amp;IX*E5D[ZN,3Y-+_F&amp;R*R?6K,;V&gt;CS?LK2&amp;6&gt;'&amp;1+""03:0P-B^!(4^L*@H[O'H0-]LRIG`9I&gt;*1"`\9L9!2V'5&amp;V8Y4O3[!_;$VP78=07L?U!]%-5$#W._=Q\BI--DA_']Z544C$$?$)(&amp;C!B0$SO^3FC^5AJB(%]UIB8!,R&amp;(!Q#FWV\.:CQ0E3"X=$HQ4&amp;@8^`%-`039%SH9$^&lt;EJ#1!Q[W=/K(HD2/UA/A!^')'B@T@489DP,"E,LL%76JQ/"$MM&lt;"E41VC0O?*`$J^Z_@^XJ&gt;ZOJ_4O!793BBAW@\R)2JVS$ZQ7]U9$X2HPQNW,0Y@+..D9W]%;Y/NK,W-GD&lt;'3)^MBA3O]-L`$F*5O%)U-MD9Q6&gt;\Z&gt;.7`A)@?\;EC@(/1KBD!\I8=)TT(GE7-7=R^V=NDR]GGXF`N2/!=B&lt;[00V02SK*'8GWQP4]@CS\%R&lt;:?G@L6_5`&gt;6ATD\8ZL[H'7]C!7BS&gt;0/@3Y%ZDW-W!^DXH9?LYFA\*]C;"F0R.0;1DS7@'WSGI,[&lt;TN[YH&amp;W^+H`U&gt;'P?$L[#1:7H('&gt;8,DT`8,ZKV5)+[J:&lt;Y-UA7'P'&gt;#BDL@&gt;E#&lt;R"HO&amp;37&amp;#7M00!PR#/##M#@@TN]L@N@GJMD*T%TY4PF.7A]+BPQ%25%#H!!!!!!1!!!"'!!!!"!!!!!!!!!!-!!&amp;#2%B1!!!!!!!$!!!!9A!!!(*YH'.A9-A4E'$[RV$XFY&amp;*Y#O1)@W8A6H1D`%X!Q/HH]"B)-UI)!E5FPX,Q#[I$2&lt;70K,,Q1!&amp;KGS-(*)=BQ5ZQ$)=,2I-````Z`B[Z"J=R2%@/&amp;.FFDS("!!59BE!!!!!!!!%!!!!"Q!!"$)!!!!(!!!!)6^O;6^-98.U3WZP&gt;WZ0&gt;WZJ&lt;G&gt;-6E.M98.T1WRV=X2F=A!!!/18!)!!!!!!!1!)!$$`````!!%!!!!!!-A!!!!&amp;!"B!=!!)!!!!!A!!#U.P&lt;H2B;7ZF=F:*!""!=!!*#%VF&lt;H6F1G&amp;S!!!71(!!#!!!!%%!!!B4&gt;7*197ZF&lt;!!!7E"Q!!="!A!!!!%$O"AA51Z#RJ/YT[*4?5:C!!!!"."U/XV1PU#&gt;B^^*G$=BX^Q!!!!"!!!!!#"5:8.U5X2B&lt;G1A65EA18"Q&lt;'FD982J&lt;WYA47&amp;O97&gt;F=A!!+%"1!!1!!!!"!!)!!RB$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-!!!%!"!!!!!!!!!!&lt;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'FN:8.U97VQ!!!!'2=!A!!!!!!"!!5!"Q!!!1!!VUW_K!!!!!!!!!!G4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B4'&amp;T&gt;%&amp;Q='RJ:725;7VF=X2B&lt;8!!!!!:&amp;Q#!!!!!!!%!"1!(!!!"!!$84&lt;[I!!!!!!!!!"J-6E.M98.T5(*J&gt;G&amp;U:52B&gt;'&amp;5?8"F2'6T9Q!!!/18!)!!!!!!!1!)!$$`````!!%!!!!!!-A!!!!&amp;!"B!=!!)!!!!!A!!#U.P&lt;H2B;7ZF=F:*!""!=!!*#%VF&lt;H6F1G&amp;S!!!71(!!#!!!!%%!!!B4&gt;7*197ZF&lt;!!!7E"Q!!="!A!!!!%$O"AA51Z#RJ/YT[*4?5:C!!!!"."U/XV1PU#&gt;B^^*G$=BX^Q!!!!"!!!!!#"5:8.U5X2B&lt;G1A65EA18"Q&lt;'FD982J&lt;WYA47&amp;O97&gt;F=A!!+%"1!!1!!!!"!!)!!RB$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-!!!%!"!!!!!!!!!!?4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'&amp;4;8JF!!!!'2=!A!!!!!!"!!5!!Q!!!1!!!!!!%!!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B2':M&gt;%2B&gt;'%!!!$A&amp;Q#!!!!!!!5!'%"Q!!A!!!!#!!!,1W^O&gt;'&amp;J&lt;G6S6EE!%%"Q!!E)476O&gt;76#98)!!":!=!!)!!!!11!!#&amp;.V9F"B&lt;G6M!!";1(!!"Q%#!!!!!1/Y'#"2$E,'E\D0IF.Z2G)!!!!%U(1\@6#`1*W(XUG9.S(@X!!!!!%!!!!!)&amp;2F=X24&gt;'&amp;O:#"633""=("M;7.B&gt;'FP&lt;C".97ZB:W6S!!!I1&amp;!!"!!!!!%!!A!$'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!!1!%!!!!!!!!!!!!!!!!K)!!%1!!!!!!!!!;4&amp;:$&lt;'&amp;T=V"S;8:B&gt;'6%982B6'&amp;C4X*E:8)!!!!V&amp;Q#!!!!!!!)!"1!(!!!-!%!!!@````]!!!!"!!%!!!!%!!!!!!!!!!%!!!!#!!!!!Q!!!!!!!!!!!!1!"Q!0!!!!"!!!!'9!!!!I!!!!!A!!"!!!!!!(!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!:Q!!!+X?*S&gt;5DV0!E%1@8$!X1EC!C*_I'NPD)G&amp;*D;H*C15*+?IB:5,NU&gt;),H=%&amp;K+&amp;#:U`Q-&lt;'RMK@1+7&gt;L89W*PQ&amp;`1=/RQE&amp;F7:SN\NPXOZ\-RE!$(GD#1V!')A@?;\E$6?UTEN)%;ZL:?&amp;WR#&amp;P!&lt;G!&gt;Q"IF5\6Z+ZQA!N#V2$&gt;25DJZ^FR]P$VLP`W7,EO6AG,P-P^'`0&amp;?,A&gt;F/ZX.Q;@1RZ^9+?C,3O3OR9\+\'$:N.JV,BM?#YL=Z@8"?H:_0YI0T].H?8(PI,MFN/N/&lt;T&gt;TEYF;N,"NG%CYCO&amp;I23/H%Z&lt;CB&lt;T&lt;/&lt;@9=V7I]OF9";8(&amp;%+T-!C^J&gt;`5))D&gt;K"#.V1INF/(:P15\U3/!/(:V"W4K$JRYUL(MJ(!O)O)U6Y@)^1P\/&amp;@85,;LW173=QBJ=ALC`2-UEBA(JN`KJ+UIW3-@P3SBMOB,)LU'%-7#X\F5?3IZ._)"D'.4$+,@IT;&amp;5+?8OQ&amp;"F5MD`%6WN6]P:"P/$=:/*U'C9UG;9&amp;S-=,4S'!*KSA1IT!K$GO%:7A.9XVI`Q?$EXV&gt;!!!!:1!"!!)!!Q!%!!!!3!!0"!!!!!!0!.A!V1!!!&amp;%!$Q1!!!!!$Q$9!.5!!!";!!]%!!!!!!]!W!$6!!!!9Y!!B!#!!!!0!.A!V1B4:7&gt;P:3"631B4:7&gt;P:3"631B4:7&gt;P:3"631%Q!!!!5F.31QU+!!.-6E.$4%*76Q!!&amp;(!!!!1&gt;!!!!)!!!&amp;&amp;!!!!!!!!!!!!!!!#!!!!!U!!!%#!!!!"N-35*/!!!!!!!!!62-6F.3!!!!!!!!!7B36&amp;.(!!!!!!!!!8R$1V.5!!!!!!!!!:"-38:J!!!!!!!!!;2$4UZ1!!!!!!!!!&lt;B544AQ!!!!!!!!!=R%2E24!!!!!!!!!?"-372T!!!!!!!!!@2735.%!!!!!!!!!ABW:8*T!!!!"!!!!BR41V.3!!!!!!!!!I"(1V"3!!!!!!!!!J2*1U^/!!!!!!!!!KBJ9WQY!!!!!!!!!LR-37:Q!!!!!!!!!N"'5%BC!!!!!!!!!O2'5&amp;.&amp;!!!!!!!!!PB75%21!!!!!!!!!QR-37*E!!!!!!!!!S"#2%BC!!!!!!!!!T2#2&amp;.&amp;!!!!!!!!!UB73624!!!!!!!!!VR%6%B1!!!!!!!!!X".65F%!!!!!!!!!Y2)36.5!!!!!!!!!ZB71V21!!!!!!!!![R'6%&amp;#!!!!!!!!!]!!!!!!`````Q!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#1!!!!!!!!!!0````]!!!!!!!!!S!!!!!!!!!!!`````Q!!!!!!!!$=!!!!!!!!!!$`````!!!!!!!!!/1!!!!!!!!!!0````]!!!!!!!!"%!!!!!!!!!!!`````Q!!!!!!!!%9!!!!!!!!!!$`````!!!!!!!!!5A!!!!!!!!!!0````]!!!!!!!!"H!!!!!!!!!!!`````Q!!!!!!!!'M!!!!!!!!!!4`````!!!!!!!!![Q!!!!!!!!!"`````]!!!!!!!!$Q!!!!!!!!!!)`````Q!!!!!!!!01!!!!!!!!!!H`````!!!!!!!!!_1!!!!!!!!!#P````]!!!!!!!!$^!!!!!!!!!!!`````Q!!!!!!!!1)!!!!!!!!!!$`````!!!!!!!!"#!!!!!!!!!!!0````]!!!!!!!!%.!!!!!!!!!!!`````Q!!!!!!!!3Y!!!!!!!!!!$`````!!!!!!!!#,Q!!!!!!!!!!0````]!!!!!!!!)T!!!!!!!!!!!`````Q!!!!!!!!V!!!!!!!!!!!$`````!!!!!!!!$5A!!!!!!!!!!0````]!!!!!!!!.5!!!!!!!!!!!`````Q!!!!!!!!VA!!!!!!!!!!$`````!!!!!!!!$=A!!!!!!!!!!0````]!!!!!!!!.U!!!!!!!!!!!`````Q!!!!!!!"))!!!!!!!!!!$`````!!!!!!!!%B!!!!!!!!!!!0````]!!!!!!!!3'!!!!!!!!!!!`````Q!!!!!!!"*%!!!!!!!!!)$`````!!!!!!!!%_1!!!!!&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O9X2M!!!!!!</Property>
	<Property Name="NI.LVClass.Geneology" Type="Xml"><String>

<Name></Name>

<Val>!!!!!2B$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!!!!!!!(!!%!!!!!!!!!!!!!!1!'!&amp;!!!!!"!!!!!!!!!!!!!!%/4'&amp;C6EF&amp;6S"09GJF9X1!5&amp;2)-!!!!!!!!!!!!")!A!1!!!!!!!!"``]!!!!"!!!!!!!"!!!!!!%!"A"1!!!!!1!!!!!!!@````Y!!!!!!2&amp;#98.F17.U&lt;X)O&lt;(:D&lt;'&amp;T=V"53$!!!!!!!!!!!!!3!)!%!!!!!!!!!1!!!!%!!!!!!!)!!!!!!A!91(!!#!!!!!)!!!N$&lt;WZU97FO:8*731"A!0(8"&gt;W"!!!!!BB$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-51W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=CZD&gt;'Q!+E"1!!%!!"V$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!%!!!!"`````Q!!!!!!!!!!!!!"%5*B=W6"9X2P=CZM&gt;G.M98.T5&amp;2)-!!!!!!!!!!!!")!A!1!!!!!!!!"!!!!!1!!!!!!!Q!!!!!#!"B!=!!)!!!!!A!!#U.P&lt;H2B;7ZF=F:*!'!!]&gt;=&amp;X9%!!!!#'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=R2$&lt;WZU97FO:8*.97ZB:W6S,G.U&lt;!!K1&amp;!!!1!!(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!1!!!!(````_!!!!!!!!!!!!!!%55'&amp;O:7R.97ZB:W6S,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!")!A!1!!!!!!!!!!!!!!1!!!!!!"!!!!!!$!"B!=!!)!!!!!A!!#U.P&lt;H2B;7ZF=F:*!""!=!!*#%VF&lt;H6F1G&amp;S!!"C!0(8"J&lt;S!!!!!BB$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-51W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=CZD&gt;'Q!,%"1!!)!!!!"(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!A!!!!)!!!!!`````Q!!!!!!!!!!!!!!!!%55'&amp;O:7R.97ZB:W6S,GRW9WRB=X-!5&amp;2)-!!!!!!!!!!!!")!A!1!!!!!!!!!!!!!!1!!!!!!"1!!!!!%!"B!=!!)!!!!!A!!#U.P&lt;H2B;7ZF=F:*!""!=!!*#%VF&lt;H6F1G&amp;S!!!71(!!#!!!!%%!!!B4&gt;7*197ZF&lt;!!!:!$RVU!\C1!!!!)91W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=CZM&gt;G.M98.T&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O9X2M!#Z!5!!$!!!!!1!#(5.M&gt;8.U:8)A&lt;W9A9WRB=X-A=(*J&gt;G&amp;U:3"E982B!!%!!Q!!!!-!!!!!!!!!!@````]!!!!!!!!!!!!!!!!!!!!!!22197ZF&lt;%VB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!%A#!"!!!!!!!!!!!!!!"!!!!!!!'!!!!!!5!'%"Q!!A!!!!#!!!,1W^O&gt;'&amp;J&lt;G6S6EE!%%"Q!!E)476O&gt;76#98)!!":!=!!)!!!!11!!#&amp;.V9F"B&lt;G6M!!";1(!!"Q%#!!!!!1/Y'#"2$E,'E\D0IF.Z2G)!!!!%U(1\@6#`1*W(XUG9.S(@X!!!!!%!!!!!)&amp;2F=X24&gt;'&amp;O:#"633""=("M;7.B&gt;'FP&lt;C".97ZB:W6S!!"G!0(84&lt;[I!!!!!BB$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-51W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=CZD&gt;'Q!-%"1!!1!!!!"!!)!!RV$&lt;(6T&gt;'6S)'^G)'.M98.T)("S;8:B&gt;'5A:'&amp;U91!"!!1!!!!%!!!!!!!!!!%!!!!#`````Q!!!!!!!!!!!!!!!+C!!"%!!!!!!22197ZF&lt;%VB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!!!!!!!!!%A#!"!!!!!!!!!!!!!!</Val>

</String>

</Property>
	<Property Name="NI.LVClass.IsTransferClass" Type="Bool">false</Property>
	<Property Name="NI.LVClass.LowestCompatibleVersion" Type="Str">1.0.0.6</Property>
	<Property Name="NI.LVClass.ParentClassLinkInfo" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!"+!!!!!22197ZF&lt;%VB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q"16%AQ!!!!+!!"!!1!!!R197ZF&lt;%VB&lt;G&amp;H:8)55'&amp;O:7R.97ZB:W6S,GRW9WRB=X-!!!!!</Property>
	<Property Name="NI.SortType" Type="Int">3</Property>
	<Item Name="ContainerManager.ctl" Type="Class Private Data" URL="ContainerManager.ctl">
		<Property Name="NI.LibItem.Scope" Type="Int">2</Property>
	</Item>
	<Item Name="InterfaceVIs" Type="Folder">
		<Item Name="MenueActivation.vi" Type="VI" URL="../MenueActivation.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;+!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!!Z!=!!*"UVF&lt;H63:79!/%"Q!"Y!!"I91W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=CZM&gt;G.M98.T!!!41W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!)!!!!%A!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		</Item>
		<Item Name="MenueSelect.vi" Type="VI" URL="../MenueSelect.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'I!!!!$1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!""!-0````](382F&lt;62B:Q!31$$`````#%FU:7V1982I!!!/1(!!#1&gt;.:7ZV5G6G!%E!]&gt;=&amp;XT1!!!!#'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=QV.:7ZV:52B&gt;'%O9X2M!"J!5!!$!!=!#!!*#5VF&lt;H6F2'&amp;U91!Y1(!!(A!!'BB$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-!!".$&lt;WZU97FO:8*.97ZB:W6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!#A!,!A!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!A!!!#3!!!!!!%!$!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1082130944</Property>
		</Item>
		<Item Name="SetRunTimeMenue.vi" Type="VI" URL="../SetRunTimeMenue.vi">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;,!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"R!-P````]35H6O,62J&lt;75A476O&gt;3"1982I!!!Y1(!!(A!!'BB$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-!!".$&lt;WZU97FO:8*.97ZB:W6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!'!!1!"Q!)!Q!!?!!!$1A!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!+!!!!!!!!!!A!!!#1!!!!!!%!#1!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1342710288</Property>
		</Item>
	</Item>
	<Item Name="TypeDefs" Type="Folder">
		<Item Name="MenueData.ctl" Type="VI" URL="../MenueData.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
		<Item Name="PrelaunchStates.ctl" Type="VI" URL="../PrelaunchStates.ctl">
			<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!!-!!!!!1!%!!!!!1!!!!!!!!</Property>
			<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
			<Property Name="NI.ClassItem.Flags" Type="Int">4194304</Property>
			<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
			<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
			<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
			<Property Name="NI.ClassItem.State" Type="Int">1074541056</Property>
		</Item>
	</Item>
	<Item Name="Properties" Type="Folder">
		<Item Name="MenueBar" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">MenueBar</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">MenueBar</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read MenueBar.vi" Type="VI" URL="../Properties/Read MenueBar.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;-!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!""!=!!*#%VF&lt;H6F1G&amp;S!!![1(!!(A!!'BB$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-!!"2$&lt;WZU97FO:8*.97ZB:W6S)'^V&gt;!!!)%"1!!-!!!!"!!)4:8*S&lt;X)A;7YA+'ZP)'6S=G^S+1!Y1(!!(A!!'BB$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-!!".$&lt;WZU97FO:8*.97ZB:W6S)'FO!'%!]!!-!!-!"!!&amp;!!9!"!!%!!1!"!!(!!1!"!!)!A!!?!!!$1A!!!!!!!!*!!!!$1M!!!!!!!!!!!!!!!!!!!!!!!!)!!!!!!!!!!!!!!!1!!!.!!!!$!!!!!!!!!!!!!!"!!E!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
		<Item Name="ContainerVI" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">ContainerVI</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">ContainerVI</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Read ContainerVI.vi" Type="VI" URL="../Properties/Read ContainerVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!"B!=!!)!!!!!A!!#U.P&lt;H2B;7ZF=F:*!$J!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!%U.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A;7Y!91$Q!!Q!!Q!%!!5!"A!%!!1!"!!%!!=!"!!%!!A#!!"Y!!!.#!!!!!!!!!E!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
			<Item Name="Write ContainerVI.vi" Type="VI" URL="../Properties/Write ContainerVI.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;5!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"B!=!!)!!!!!A!!#U.P&lt;H2B;7ZF=F:*!$B!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!%U.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
				<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
			</Item>
		</Item>
		<Item Name="MenueBarVisble" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">MenueBarVisble</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">MenueBarVisble</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Write MenueBarVisible.vi" Type="VI" URL="../Properties/Write MenueBarVisible.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!&amp;1!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!"2!)1Z.:7ZV:5*B=F:J=W*M:1!!/%"Q!"Y!!"I91W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=CZM&gt;G.M98.T!!!41W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=C"J&lt;A"B!0!!$!!$!!1!"!!&amp;!!1!"!!%!!1!"A!%!!=!#!-!!(A!!!U)!!!!!!!!!!!!!!U,!!!!!!!!!!!!!!!!!!!!!!!!#A!!!!!!!!!+!!!!%!!!$1!!!!Q!!!!!!!!!!!!!!1!*!!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
			</Item>
		</Item>
		<Item Name="TestStand UI Application Manager" Type="Property Definition">
			<Property Name="NI.ClassItem.Property.LongName" Type="Str">TestStand UI Application Manager</Property>
			<Property Name="NI.ClassItem.Property.ShortName" Type="Str">TestStand UI Application Manager</Property>
			<Property Name="NI.ClassItem.Property.SortKey" Type="Int">-1</Property>
			<Item Name="Write TestStand UI Application Manager.vi" Type="VI" URL="../Properties/Write TestStand UI Application Manager.vi">
				<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!'7!!!!#A!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!&amp;J!=!!(!1)!!!!"!\A9)&amp;%/1M;4O-_C5XF'9A!!!!41&gt;$N^5,^!H9@@3:AX)&gt;`=!!!!!1!!!!!A6'6T&gt;&amp;.U97ZE)&amp;6*)%&amp;Q='RJ9W&amp;U;7^O)%VB&lt;G&amp;H:8)!!$B!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!%U.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!(!!A#!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!%!!!!")!!!U!!!!-!!!!!!!!!!!!!!%!#1!!!!!</Property>
				<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
				<Property Name="NI.ClassItem.Flags" Type="Int">16777216</Property>
				<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
				<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
				<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
				<Property Name="NI.ClassItem.State" Type="Int">1107820544</Property>
			</Item>
		</Item>
	</Item>
	<Item Name="ScaleFrontPanel.vi" Type="VI" URL="../ScaleFrontPanel.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%]!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!%U.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A;7Y!91$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!.#!!!!!!!!!!!!!!.#Q!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!"!!!!U!!!!-!!!!!!!!!!!!!!%!#!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">true</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107821072</Property>
	</Item>
	<Item Name="PanelClose.vi" Type="VI" URL="../PanelClose.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%P!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A&lt;X6U!!!A1&amp;!!!Q!!!!%!!B.F=H*P=C"J&lt;C!I&lt;G]A:8*S&lt;X)J!$B!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!%U.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=#!!"Y!!!.#!!!!!!!!!!!!!#.#Q!!!!!!!!!!!!!!!!!!!!!!!!A!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">0</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">1</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1342710272</Property>
	</Item>
	<Item Name="Actor Core.vi" Type="VI" URL="../Actor Core.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%D!!!!#1!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$B!=!!?!!!F&amp;5&amp;D&gt;'^S)%:S97VF&gt;W^S;SZM&gt;GRJ9AV"9X2P=CZM&gt;G.M98.T!!F"9X2P=C"P&gt;81!&amp;E"1!!-!!!!"!!)):8*S&lt;X)A;7Y!!$B!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!%U.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A;7Y!6!$Q!!Q!!Q!%!!1!"1!%!!1!"!!%!!9!"!!%!!=$!!"Y!!!*!!!!!!!!!!!!!!!*!!!!!!!!!!!!!!!!!!!!!!!!!!I!!!!!!!!!!!!!!*!!!!!!!1!)!!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">1090519424</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1107825168</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Pre Launch Init.vi" Type="VI" URL="../Pre Launch Init.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!%0!!!!#!!-1#%'=X2B&gt;(6T!!!,1!-!"'.P:'5!!""!-0````]'=W^V=G.F!!!71&amp;!!!Q!!!!%!!AFF=H*P=C"P&gt;81!"!!!!$J!=!!?!!!;'%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)O&lt;(:D&lt;'&amp;T=Q!!&amp;%.P&lt;H2B;7ZF=EVB&lt;G&amp;H:8)A&lt;X6U!!!Y1(!!(A!!'BB$&lt;WZU97FO:8*.97ZB:W6S,GRW9WRB=X-!!".$&lt;WZU97FO:8*.97ZB:W6S)'FO!&amp;1!]!!-!!-!"!!%!!5!"!!%!!1!"!!%!!1!"!!'!Q!!?!!!#1!!!!!!!!!!!!!!D1M!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!#3!!!!!!%!"Q!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
	<Item Name="Stop Core.vi" Type="VI" URL="../Stop Core.vi">
		<Property Name="NI.ClassItem.ConnectorPane" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!$J!!!!"1!%!!!!/E"Q!"Y!!"I91W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=CZM&gt;G.M98.T!!!51W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=C"P&gt;81!!"&gt;!!Q!1:GFO97QA:8*S&lt;X)A9W^E:1!!/%"Q!"Y!!"I91W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=CZM&gt;G.M98.T!!!41W^O&gt;'&amp;J&lt;G6S47&amp;O97&gt;F=C"J&lt;A"5!0!!$!!!!!!!!!!"!!!!!!!!!!!!!!!!!!)!!Q-!!(A!!!!!!!!!!!!!!!!!!)E!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!1!!!!E!!!!!!"!!1!!!!!</Property>
		<Property Name="NI.ClassItem.ExecutionSystem" Type="Int">-1</Property>
		<Property Name="NI.ClassItem.Flags" Type="Int">16777344</Property>
		<Property Name="NI.ClassItem.IsStaticMethod" Type="Bool">false</Property>
		<Property Name="NI.ClassItem.MethodScope" Type="UInt">3</Property>
		<Property Name="NI.ClassItem.Priority" Type="Int">1</Property>
		<Property Name="NI.ClassItem.State" Type="Int">1082130960</Property>
		<Property Name="NI.LibItem.Scope" Type="Int">3</Property>
	</Item>
</LVClass>
