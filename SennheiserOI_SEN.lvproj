﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="CCSymbols" Type="Str">SEN_DEBUG,FALSE;SEN_TESTSTANDFP,FALSE;AF_Debug_Trace,False;ModelType,INIFile;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">false</Property>
	<Property Name="NI.Project.Description" Type="Str"></Property>
	<Item Name="Mein Computer" Type="My Computer">
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">Mein Computer/VI-Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">Mein Computer/VI-Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="SENAFLV17TS2017" Type="Folder">
			<Item Name="Actors" Type="Folder">
				<Item Name="Messages" Type="Folder">
					<Item Name="BaseActorMessages" Type="Folder">
						<Item Name="TriggerUpdate Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/BaseActor Messages/TriggerUpdate Msg/TriggerUpdate Msg.lvclass"/>
						<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
					</Item>
					<Item Name="TestStandActorMessages" Type="Folder">
						<Item Name="AppMgrStop Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TeststandActorV2 Messages/AppMgrStop Msg/AppMgrStop Msg.lvclass"/>
						<Item Name="AppMgrStart Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TeststandActorV2 Messages/AppMgrStart Msg/AppMgrStart Msg.lvclass"/>
						<Item Name="FindSeqFile Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TeststandActorV2 Messages/FindSeqFile Msg/FindSeqFile Msg.lvclass"/>
						<Item Name="LogIn Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TeststandActorV2 Messages/LogIn Msg/LogIn Msg.lvclass"/>
						<Item Name="OpenSequenceFile Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TeststandActorV2 Messages/OpenSequenceFile Msg/OpenSequenceFile Msg.lvclass"/>
						<Item Name="NewExecution Msg.lvclass" Type="LVClass" URL="../SEN/Classes/TestStand Messages/NewExecution Msg/NewExecution Msg.lvclass"/>
					</Item>
					<Item Name="TestSocketMessages" Type="Folder">
						<Item Name="SetPreload Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetPreload Msg/SetPreload Msg.lvclass"/>
						<Item Name="SetTab Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetTab Msg/SetTab Msg.lvclass"/>
						<Item Name="SetButtonAppearance Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetButtonAppearance Msg/SetButtonAppearance Msg.lvclass"/>
						<Item Name="SetControlAppearance Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetControlAppearance Msg/SetControlAppearance Msg.lvclass"/>
						<Item Name="SetInfo Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetInfo Msg/SetInfo Msg.lvclass"/>
						<Item Name="SetPicture Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetPicture Msg/SetPicture Msg.lvclass"/>
						<Item Name="SetRemark Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetRemark Msg/SetRemark Msg.lvclass"/>
						<Item Name="SetState Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetState Msg/SetState Msg.lvclass"/>
						<Item Name="SetViewMode Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetViewMode Msg/SetViewMode Msg.lvclass"/>
						<Item Name="DisplaySequenceFileName Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetSequenceFile Msg/DisplaySequenceFileName Msg.lvclass"/>
						<Item Name="SetCounter Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetCounter Msg/SetCounter Msg.lvclass"/>
						<Item Name="SetProgress Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetProgress Msg/SetProgress Msg.lvclass"/>
						<Item Name="SetImagePath Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetImagePath Msg/SetImagePath Msg.lvclass"/>
						<Item Name="SetDSR Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetDSR Msg/SetDSR Msg.lvclass"/>
						<Item Name="SetPrestart Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetPrestart Msg/SetPrestart Msg.lvclass"/>
						<Item Name="UIEvent Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/UIEvent Msg/UIEvent Msg.lvclass"/>
						<Item Name="PanelClose Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/ContainerManager Messages/PanelClose Msg/PanelClose Msg.lvclass"/>
						<Item Name="SetRunning Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetRunning Msg/SetRunning Msg.lvclass"/>
						<Item Name="DisplayReport Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/DisplayReport Msg/DisplayReport Msg.lvclass"/>
					</Item>
					<Item Name="ContainerManager" Type="Folder">
						<Item Name="SetRunTimeMenue Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/ContainerManager Messages/SetRunTimeMenue Msg/SetRunTimeMenue Msg.lvclass"/>
						<Item Name="MenueSelect Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/ContainerManager Messages/MenueSelect Msg/MenueSelect Msg.lvclass"/>
						<Item Name="MenueActivation Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/ContainerManager Messages/MenueActivation Msg/MenueActivation Msg.lvclass"/>
					</Item>
					<Item Name="ControllerMessages" Type="Folder">
						<Item Name="OpenCloseSequenceFile Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/ControllerActor Messages/OpenCloseSequenceFile Msg/OpenCloseSequenceFile Msg.lvclass"/>
						<Item Name="SetSequenceFile Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/ControllerActor Messages/SetSequenceFile Msg/SetSequenceFile Msg.lvclass"/>
						<Item Name="Parallel Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/ControllerActor Messages/Parallel Msg/Parallel Msg.lvclass"/>
						<Item Name="SetUserType Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/ControllerActor Messages/SetUserType Msg/SetUserType Msg.lvclass"/>
						<Item Name="SetControllerState Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/ControllerActor Messages/SetControllerState Msg/SetControllerState Msg.lvclass"/>
					</Item>
				</Item>
				<Item Name="TestSocketActor" Type="Folder">
					<Item Name="Launcher" Type="Folder">
						<Item Name="TestsocketLauncher.vi" Type="VI" URL="../SEN/Classes/Testsocket/Properties/Info/TestsocketLauncher.vi"/>
						<Item Name="LauncherControlObject.ctl" Type="VI" URL="../SEN/Classes/Testsocket/Properties/Info/LauncherControlObject.ctl"/>
						<Item Name="LauncherControl.ctl" Type="VI" URL="../SEN/Classes/Testsocket/Properties/Info/LauncherControl.ctl"/>
					</Item>
					<Item Name="TestSocket.lvclass" Type="LVClass" URL="../SEN/SENAF/Testsocket/TestSocket.lvclass"/>
				</Item>
				<Item Name="Teststand" Type="Folder">
					<Item Name="Launcher" Type="Folder">
						<Item Name="TeststandLauncher.vi" Type="VI" URL="../SEN/Classes/TestStandV2/Launcher/TeststandLauncher.vi"/>
					</Item>
					<Item Name="TeststandActorV2.lvclass" Type="LVClass" URL="../SEN/SENAF/TeststandV2/TeststandActorV2.lvclass"/>
				</Item>
				<Item Name="Parent" Type="Folder">
					<Item Name="UIAtom" Type="Folder">
						<Item Name="UIAtomManager.lvclass" Type="LVClass" URL="../SEN/SENAF/UIAtom/UIAtomManager.lvclass"/>
					</Item>
					<Item Name="TextBox" Type="Folder">
						<Item Name="Launcher" Type="Folder">
							<Item Name="TextboxLauncher.vi" Type="VI" URL="../SEN/SENAF/TextBox/Launcher/TextboxLauncher.vi"/>
						</Item>
						<Item Name="Messages" Type="Folder">
							<Item Name="Clear Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/Textbox Messages/Clear Msg/Clear Msg.lvclass"/>
							<Item Name="SetStyle Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/Textbox Messages/SetStyle Msg/SetStyle Msg.lvclass"/>
							<Item Name="SetColor Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/Textbox Messages/SetColor Msg/SetColor Msg.lvclass"/>
							<Item Name="WriteText Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/Textbox Messages/WriteText Msg/WriteText Msg.lvclass"/>
							<Item Name="WriteUnformattedText Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/Textbox Messages/WriteUnformattedText Msg/WriteUnformattedText Msg.lvclass"/>
						</Item>
						<Item Name="Textbox.lvclass" Type="LVClass" URL="../SEN/SENAF/TextBox/Textbox.lvclass"/>
					</Item>
					<Item Name="ContainerManager" Type="Folder">
						<Item Name="Container.vit" Type="VI" URL="../SEN/SENAF/ContainerManager/Container.vit"/>
						<Item Name="ContainerManager.lvclass" Type="LVClass" URL="../SEN/SENAF/ContainerManager/ContainerManager.lvclass"/>
					</Item>
					<Item Name="PanelManager" Type="Folder">
						<Item Name="PanelManager.lvclass" Type="LVClass" URL="../SEN/SENAF/PanelManager/PanelManager.lvclass"/>
					</Item>
					<Item Name="Base" Type="Folder">
						<Item Name="BaseActor.lvclass" Type="LVClass" URL="../SEN/SENAF/Base/BaseActor.lvclass"/>
					</Item>
					<Item Name="StaticContainerVIActor" Type="Folder">
						<Item Name="StaticContainerVIActor.lvclass" Type="LVClass" URL="../SEN/SENAF/StaticContainerVIActor/StaticContainerVIActor.lvclass"/>
					</Item>
				</Item>
				<Item Name="Obsolete" Type="Folder"/>
				<Item Name="Controller" Type="Folder">
					<Item Name="Launcher" Type="Folder">
						<Item Name="Launcher.vi" Type="VI" URL="../SEN/Classes/Testsocket/Properties/Launcher.vi"/>
					</Item>
					<Item Name="ControllerActor.lvclass" Type="LVClass" URL="../SEN/SENAF/Controller/ControllerActor.lvclass"/>
				</Item>
			</Item>
		</Item>
		<Item Name="SennheiserOI" Type="Folder">
			<Item Name="ControllerV2" Type="Folder">
				<Item Name="Messages" Type="Folder">
					<Item Name="SwitchUser Msg.lvclass" Type="LVClass" URL="../SEN/Classes/Controller Messages/SwitchUser Msg/SwitchUser Msg.lvclass"/>
					<Item Name="StartTestSocketUI Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/ControllerActor Messages/StartTestSocketUI Msg/StartTestSocketUI Msg.lvclass"/>
					<Item Name="RunSequence Msg.lvclass" Type="LVClass" URL="../SEN/Classes/Controller Messages/RunSequence Msg/RunSequence Msg.lvclass"/>
					<Item Name="DisplaySequenceFile Msg.lvclass" Type="LVClass" URL="../SEN/Classes/Controller Messages/DisplaySequenceFile Msg/DisplaySequenceFile Msg.lvclass"/>
					<Item Name="SequenceFileClosed Msg.lvclass" Type="LVClass" URL="../SEN/Classes/Controller Messages/SequenceFileClosed Msg/SequenceFileClosed Msg.lvclass"/>
					<Item Name="UserChangedAction Msg.lvclass" Type="LVClass" URL="../SEN/Classes/Controller Messages/UserChangedAction Msg/UserChangedAction Msg.lvclass"/>
					<Item Name="UIMessageAction Msg.lvclass" Type="LVClass" URL="../SEN/Classes/Controller Messages/UIMessageAction Msg/UIMessageAction Msg.lvclass"/>
					<Item Name="DisplayExecutionAction Msg.lvclass" Type="LVClass" URL="../SEN/Classes/Controller Messages/DisplayExecutionAction Msg/DisplayExecutionAction Msg.lvclass"/>
				</Item>
				<Item Name="Controller.lvclass" Type="LVClass" URL="../SEN/Classes/ControllerV2/Controller.lvclass"/>
			</Item>
			<Item Name="TestStandV2" Type="Folder">
				<Item Name="TestStand.lvclass" Type="LVClass" URL="../SEN/Classes/TestStandV2/TestStand.lvclass"/>
			</Item>
			<Item Name="Testsocket" Type="Folder">
				<Item Name="Messages" Type="Folder">
					<Item Name="Tools" Type="Folder">
						<Item Name="SetControl.vi" Type="VI" URL="../SEN/SENAF/Testsocket/SetControl.vi"/>
					</Item>
					<Item Name="HandleExecutionViewManagerEvent Msg.lvclass" Type="LVClass" URL="../SEN/Classes/TestsocketActor Messages/HandleExecutionViewManagerEvent Msg/HandleExecutionViewManagerEvent Msg.lvclass"/>
					<Item Name="SetPaused Msg.lvclass" Type="LVClass" URL="../SEN/SENAF/TestSocket Messages/SetPaused Msg/SetPaused Msg.lvclass"/>
					<Item Name="Write Sequence Context Msg.lvclass" Type="LVClass" URL="../SEN/Classes/TestsocketActor Messages/Write Sequence Context Msg/Write Sequence Context Msg.lvclass"/>
				</Item>
				<Item Name="TestsocketActor.lvclass" Type="LVClass" URL="../SEN/Classes/Testsocket/TestsocketActor.lvclass"/>
			</Item>
			<Item Name="Tools" Type="Folder">
				<Item Name="Hostname.vi" Type="VI" URL="../SEN/Classes/Teststand/Tools/Hostname.vi"/>
				<Item Name="ClearSpecificError.vi" Type="VI" URL="../SEN/ClearSpecificError.vi"/>
				<Item Name="CurrentUserLoginName.vi" Type="VI" URL="../SEN/Classes/Teststand/Tools/CurrentUserLoginName.vi"/>
				<Item Name="UserInGroup.vi" Type="VI" URL="../SEN/Classes/Teststand/Tools/UserInGroup.vi"/>
				<Item Name="ColorDistribution.vi" Type="VI" URL="../SEN/ColorDistribution.vi"/>
			</Item>
		</Item>
		<Item Name="TopLevel" Type="Folder">
			<Item Name="Typedef" Type="Folder">
				<Item Name="TopLevelStates.ctl" Type="VI" URL="../SennheiserOI/TypDef/TopLevelStates.ctl"/>
				<Item Name="StationGlobals.ctl" Type="VI" URL="../SEN/Classes/Teststand/Typedef/StationGlobals.ctl"/>
			</Item>
			<Item Name="AsynchronousPlugIns" Type="Folder">
				<Item Name="StartMaxReport.vi" Type="VI" URL="../SEN/Classes/Controller/Properties/StateMethods/StartMaxReport.vi"/>
				<Item Name="StartRelaisCount.vi" Type="VI" URL="../SEN/Classes/Controller/Properties/StateMethods/StartRelaisCount.vi"/>
			</Item>
			<Item Name="TopLevelVIwoModelSelection.vi" Type="VI" URL="../SennheiserOI/TopLevelVIwoModelSelection.vi"/>
			<Item Name="SplashScreen.vi" Type="VI" URL="../SennheiserOI/SplashScreen.vi"/>
			<Item Name="changelog.txt" Type="Document" URL="../changelog.txt"/>
		</Item>
		<Item Name="Library" Type="Folder" URL="../Library">
			<Property Name="NI.DISK" Type="Bool">true</Property>
			<Property Name="NI.SortType" Type="Int">3</Property>
		</Item>
		<Item Name="TestExec" Type="Folder" URL="../SennheiserOI/TestExec">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="PG" Type="Folder">
			<Item Name="InteractiveGetUIMessageEneumerationString.vi" Type="VI" URL="../SEN/Classes/TestStandV2/InteractiveGetUIMessageEneumerationString.vi"/>
		</Item>
		<Item Name="Abhängigkeiten" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Read JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Read JPEG File.vi"/>
				<Item Name="Picture to Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Picture to Pixmap.vi"/>
				<Item Name="TestStand - Get Property Value (Number).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Number).vi"/>
				<Item Name="TestStand - Get Property Value (String).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (String).vi"/>
				<Item Name="TestStand - Get Property Value (Number {Unsigned 16-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Number {Unsigned 16-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Number {Signed 16-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Number {Signed 16-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Number {Unsigned 32-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Number {Unsigned 32-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Number {Signed 32-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Number {Signed 32-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Number {Unsigned 8-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Number {Unsigned 8-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Number {Signed 8-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Number {Signed 8-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Numeric Array).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Numeric Array).vi"/>
				<Item Name="TestStand - Get Property Value (Numeric Array {Unsigned 32-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Numeric Array {Unsigned 32-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Numeric Array {Signed 32-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Numeric Array {Signed 32-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Numeric Array {Unsigned 16-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Numeric Array {Unsigned 16-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Numeric Array {Signed 16-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Numeric Array {Signed 16-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Numeric Array {Unsigned 8-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Numeric Array {Unsigned 8-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Numeric Array {Signed 8-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Numeric Array {Signed 8-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Number {Unsigned 64-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Number {Unsigned 64-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Number {Signed 64-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Number {Signed 64-bit Integer}).vi"/>
				<Item Name="TestStand - Validate Evaluation Types.vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Validate Evaluation Types.vi"/>
				<Item Name="TestStand API Numeric Constants.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand API Numeric Constants.ctl"/>
				<Item Name="TestStand API Numeric Constants.vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand API Numeric Constants.vi"/>
				<Item Name="TestStand - Get Property Value (Numeric Array {Unsigned 64-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Numeric Array {Unsigned 64-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (Numeric Array {Signed 64-bit Integer}).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Numeric Array {Signed 64-bit Integer}).vi"/>
				<Item Name="TestStand - Get Property Value (String Array).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (String Array).vi"/>
				<Item Name="TestStand - Get Property Value (Object).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Object).vi"/>
				<Item Name="TestStand - Get Property Value (Boolean).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Boolean).vi"/>
				<Item Name="TestStand - Get Property Value (Boolean Array).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Boolean Array).vi"/>
				<Item Name="TestStand - Get Property Value.vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value.vi"/>
				<Item Name="lvpalettesupport.dll" Type="Document" URL="/&lt;vilib&gt;/addons/TestStand/lvpalettesupport.dll"/>
				<Item Name="Write To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File.vi"/>
				<Item Name="Write To Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (DBL).vi"/>
				<Item Name="Write To Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (I64).vi"/>
				<Item Name="Write To Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (string).vi"/>
				<Item Name="System Directory Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/System Directory Type.ctl"/>
				<Item Name="nisyscfg.lvlib" Type="Library" URL="/&lt;vilib&gt;/nisyscfg/nisyscfg.lvlib"/>
				<Item Name="Get System Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/sysdir.llb/Get System Directory.vi"/>
				<Item Name="Color to RGB.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/Color to RGB.vi"/>
				<Item Name="RGB to Color.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/colorconv.llb/RGB to Color.vi"/>
				<Item Name="Read PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Read PNG File.vi"/>
				<Item Name="Draw Flattened Pixmap.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Draw Flattened Pixmap.vi"/>
				<Item Name="LVPointTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPointTypeDef.ctl"/>
				<Item Name="Get LV Class Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Path.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="imagedata.ctl" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/imagedata.ctl"/>
				<Item Name="Check Path.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Path.vi"/>
				<Item Name="Create Mask By Alpha.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Create Mask By Alpha.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Directory of Top Level VI.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Directory of Top Level VI.vi"/>
				<Item Name="Bit-array To Byte-array.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/Bit-array To Byte-array.vi"/>
				<Item Name="FixBadRect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/pictutil.llb/FixBadRect.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="TestStand - Get Property Value (Reference).vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand - Get Property Value (Reference).vi"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Is Path and Not Empty.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Is Path and Not Empty.vi"/>
				<Item Name="TestStand API String Constants.ctl" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand API String Constants.ctl"/>
				<Item Name="TestStand API String Constants.vi" Type="VI" URL="/&lt;vilib&gt;/addons/TestStand/_TSUtility.llb/TestStand API String Constants.vi"/>
				<Item Name="NI_ReportGenerationCore.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_ReportGenerationCore.lvlib"/>
				<Item Name="NI_report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/NI_report.lvclass"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Built App File Layout.vi" Type="VI" URL="/&lt;vilib&gt;/AppBuilder/Built App File Layout.vi"/>
				<Item Name="NI_Standard Report.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Standard Report/NI_Standard Report.lvclass"/>
				<Item Name="Flip and Pad for Picture Control.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Flip and Pad for Picture Control.vi"/>
				<Item Name="Calc Long Word Padded Width.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Calc Long Word Padded Width.vi"/>
				<Item Name="Write BMP Data To Buffer.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data To Buffer.vi"/>
				<Item Name="Write BMP Data.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP Data.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="Write BMP File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/bmp.llb/Write BMP File.vi"/>
				<Item Name="Check Data Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Data Size.vi"/>
				<Item Name="Check Color Table Size.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check Color Table Size.vi"/>
				<Item Name="Check File Permissions.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Check File Permissions.vi"/>
				<Item Name="Write JPEG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/jpeg.llb/Write JPEG File.vi"/>
				<Item Name="NI_HTML.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/Utility/NIReport.llb/HTML/NI_HTML.lvclass"/>
				<Item Name="Write PNG File.vi" Type="VI" URL="/&lt;vilib&gt;/picture/png.llb/Write PNG File.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="Create Error Clust.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create Error Clust.vi"/>
				<Item Name="Destroy ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Destroy ActiveX Event Queue.vi"/>
				<Item Name="OccFireType.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/OccFireType.ctl"/>
				<Item Name="EventData.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/EventData.ctl"/>
				<Item Name="Wait On ActiveX Event.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait On ActiveX Event.vi"/>
				<Item Name="Wait types.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Wait types.ctl"/>
				<Item Name="Create ActiveX Event Queue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/ax-events.llb/Create ActiveX Event Queue.vi"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Escape Characters for HTTP.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Escape Characters for HTTP.vi"/>
				<Item Name="Path to URL.vi" Type="VI" URL="/&lt;vilib&gt;/printing/PathToURL.llb/Path to URL.vi"/>
				<Item Name="Generate Temporary File Path.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Generate Temporary File Path.vi"/>
				<Item Name="Beep.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/Beep.vi"/>
				<Item Name="Generate Report Object Reference.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/NIReport.llb/Generate Report Object Reference.ctl"/>
			</Item>
			<Item Name="instr.lib" Type="Folder">
				<Item Name="niSwitch Initialize.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Initialize.vi"/>
				<Item Name="niSwitch Close.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Close.vi"/>
				<Item Name="niSwitch Get Relay Name.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Get Relay Name.vi"/>
				<Item Name="niSwitch Get Relay Count.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch Get Relay Count.vi"/>
				<Item Name="niSwitch IVI Error Converter.vi" Type="VI" URL="/&lt;instrlib&gt;/niSwitch/niSwitch.llb/niSwitch IVI Error Converter.vi"/>
				<Item Name="niModInst Set Error.vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Set Error.vi"/>
				<Item Name="niModInst Get Installed Device Attribute (I32).vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Get Installed Device Attribute (I32).vi"/>
				<Item Name="niModInst Open Installed Devices Session.vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Open Installed Devices Session.vi"/>
				<Item Name="niModInst Close Installed Devices Session.vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Close Installed Devices Session.vi"/>
				<Item Name="niModInst Get Installed Device Attribute (poly).vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Get Installed Device Attribute (poly).vi"/>
				<Item Name="niModInst Get Installed Device Attribute (String).vi" Type="VI" URL="/&lt;instrlib&gt;/niModInst/niModInst Get Installed Device Attribute (String).vi"/>
			</Item>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="DisplayReportCallback.vi" Type="VI" URL="../SEN/Classes/TestStandV2/Callbacks/DisplayReportCallback.vi"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="niswitch_32.dll" Type="Document" URL="niswitch_32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="GetComputerDescription.vi" Type="VI" URL="/M/Library/WindowsAPI/GetComputerDescription.vi"/>
			<Item Name="Get Computername.vi" Type="VI" URL="/M/Library/WindowsAPI/Get Computername.vi"/>
			<Item Name="Global Variable.vi" Type="VI" URL="/M/SennheiserOI/TestExec/Global Variable.vi"/>
			<Item Name="GetStepName.vi" Type="VI" URL="/M/SennheiserOI/TestExec/GetStepName.vi"/>
			<Item Name="Initialize Report.vi" Type="VI" URL="/M/SennheiserOI/TestExec/Initialize Report.vi"/>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="niModInst.dll" Type="Document" URL="niModInst.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="MasterFP.rtm" Type="Document" URL="../SEN/Classes/Documents/sennheiser_oi/SEN/Classes/Testsocket/MasterFP.rtm"/>
			<Item Name="MasterFP.rtm" Type="Document" URL="/../Documents/sennheiser_oi/SEN/Classes/Testsocket/MasterFP.rtm"/>
		</Item>
		<Item Name="Build-Spezifikationen" Type="Build">
			<Item Name="SetSequenceFileRef" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{C5E63308-B475-418C-8BCF-B1D62137B3BC}</Property>
				<Property Name="App_INI_GUID" Type="Str">{556489CD-5BE5-413E-9B2C-36491B7A11BD}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{F826BE8A-3641-45FB-959A-EBD96318C423}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">SetSequenceFileRef</Property>
				<Property Name="Bld_defaultLanguage" Type="Str">German</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/SetSequenceFileRef</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{B908B15B-0E60-46ED-8F3A-D9C2A20646AA}</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Application.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/SetSequenceFileRef/Application.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Hilfsdatei-Verzeichnis</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/SetSequenceFileRef/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Source[0].itemID" Type="Str">{429311F6-DF7C-46A9-B71B-987D8DAB7277}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">2</Property>
				<Property Name="TgtF_fileDescription" Type="Str">SetSequenceFileRef</Property>
				<Property Name="TgtF_internalName" Type="Str">SetSequenceFileRef</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017 </Property>
				<Property Name="TgtF_productName" Type="Str">SetSequenceFileRef</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{F8B82847-0049-41D5-AEDB-A47142057DF2}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Application.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
